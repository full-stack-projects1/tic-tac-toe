package com.example.tic_tac_toe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    // Player representation
    // 0 - X
    // 1 - O
    int activePlayer = 0;
    int count = 0;
    boolean gameActive = true;
    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};
    int[][] winPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void playerTap(View view) {
        if (!gameActive) {
            resetGame(view);
        }

        ImageView playNow = findViewById(R.id.playNow);
        if (count % 2 == 0) {
            playNow.setImageResource(R.drawable.oplay);
        }
        else {
            playNow.setImageResource(R.drawable.xplay);
        }

        ImageButton imageButton = (ImageButton) view;
        int tappedButton = Integer.parseInt(imageButton.getTag().toString());

        gameState[tappedButton] = activePlayer;

        ImageView winnerImage = findViewById(R.id.winner);
        int flag = 0;
        // Check if any player has won
        for (int[] winPosition : winPositions) {
            if (gameState[winPosition[0]] == gameState[winPosition[1]] &&
                    gameState[winPosition[1]] == gameState[winPosition[2]] &&
                    gameState[winPosition[0]] != 2) {

                flag = 1;
                int winner;

                if (gameState[winPosition[0]] == 0) {
                    winner = R.drawable.xwin;
                } else {
                    winner = R.drawable.owin;
                }

                winnerImage.setImageResource(winner);
                printWinLine(winPosition);
                playNow.setImageResource(R.drawable.empty);
            }
        }

        if (activePlayer == 1) {
            imageButton.setImageResource(R.drawable.o);
            activePlayer = 0;
        } else {
            imageButton.setImageResource(R.drawable.x);
            activePlayer = 1;
        }

        if (count == 8 && flag == 0) {
            gameActive = false;
            winnerImage.setImageResource(R.drawable.nowin);
            playNow.setImageResource(R.drawable.empty);
            Button playAgainButton = findViewById(R.id.playAgain);
            playAgainButton.setVisibility(View.VISIBLE);
        }

        if(flag == 1){
            gameActive = false;
            Button playAgainButton = findViewById(R.id.playAgain);
            playAgainButton.setVisibility(View.VISIBLE);
        }

        count++;
    }

    public void resetGame(View view) {
        ImageView winnerImage = findViewById(R.id.winner);
        winnerImage.setImageResource(R.drawable.empty);
        Button playAgainButton = findViewById(R.id.playAgain);
        playAgainButton.setVisibility(View.INVISIBLE);

        ImageView playNow = findViewById(R.id.playNow);
        playNow.setImageResource(R.drawable.empty);

        ((ImageButton) findViewById(R.id.imageButton11)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton12)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton13)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton21)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton22)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton23)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton31)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton32)).setImageResource(R.drawable.empty);
        ((ImageButton) findViewById(R.id.imageButton33)).setImageResource(R.drawable.empty);
        activePlayer = 0;
        gameState = new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2};
        gameActive = true;
        count = 0;

        ImageView row1 = findViewById(R.id.row1);
        row1.setVisibility(View.INVISIBLE);

        ImageView row2 = findViewById(R.id.row2);
        row2.setVisibility(View.INVISIBLE);

        ImageView row3 = findViewById(R.id.row3);
        row3.setVisibility(View.INVISIBLE);

        ImageView column1 = findViewById(R.id.column1);
        column1.setVisibility(View.INVISIBLE);

        ImageView column2 = findViewById(R.id.column2);
        column2.setVisibility(View.INVISIBLE);

        ImageView column3 = findViewById(R.id.column3);
        column3.setVisibility(View.INVISIBLE);

        ImageView diagonal1 = findViewById(R.id.diagonal1);
        diagonal1.setVisibility(View.INVISIBLE);

        ImageView diagonal2 = findViewById(R.id.diagonal2);
        diagonal2.setVisibility(View.INVISIBLE);
    }

    private void printWinLine(int[] winPosition) {
       if(winPosition == winPositions[0]){
           ImageView row1 = findViewById(R.id.row1);
           row1.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[1]) {
           ImageView row2 = findViewById(R.id.row2);
           row2.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[2]) {
           ImageView row3 = findViewById(R.id.row3);
           row3.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[3]) {
           ImageView column1 = findViewById(R.id.column1);
           column1.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[4]) {
           ImageView column2 = findViewById(R.id.column2);
           column2.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[5]) {
           ImageView column3 = findViewById(R.id.column3);
           column3.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[6]) {
           ImageView diagonal1 = findViewById(R.id.diagonal1);
           diagonal1.setVisibility(View.VISIBLE);
       } else if(winPosition == winPositions[7]) {
           ImageView diagonal2 = findViewById(R.id.diagonal2);
           diagonal2.setVisibility(View.VISIBLE);
       }
    }
}